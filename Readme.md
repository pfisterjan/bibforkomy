# BibSonomy

[BibSonomy](https://www.bibsonomy.org/) is a social bookmark and publication sharing system. 

It is developed and operated by 
the [Data Science Chair](https://www.informatik.uni-wuerzburg.de/datascience/home/) at the University of Würzburg, Germany,
the [Information Processing and Analytics Group](https://www.ibi.hu-berlin.de/en/research/Information-processing/) at the Humboldt-Universität zu Berlin, Germany,
the [Knowledge & Data Engineering Group](https://www.kde.cs.uni-kassel.de/) at the University of Kassel, Germany, and
the [L3S Research Center](https://www.l3s.de/) at Leibniz University Hannover, Germany.


More information can be found in the [BibSonomy Wiki](https://bitbucket.org/bibsonomy/bibsonomy/wiki/Home).

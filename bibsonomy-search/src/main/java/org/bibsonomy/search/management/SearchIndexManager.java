/**
 * BibSonomy Search - Helper classes for search modules.
 *
 * Copyright (C) 2006 - 2021 Data Science Chair,
 *                               University of Würzburg, Germany
 *                               https://www.informatik.uni-wuerzburg.de/datascience/home/
 *                           Information Processing and Analytics Group,
 *                               Humboldt-Universität zu Berlin, Germany
 *                               https://www.ibi.hu-berlin.de/en/research/Information-processing/
 *                           Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               https://www.kde.cs.uni-kassel.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               https://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.search.management;

import java.util.List;

import org.bibsonomy.model.Resource;
import org.bibsonomy.search.exceptions.IndexAlreadyGeneratingException;
import org.bibsonomy.search.model.SearchIndexInfo;

/**
 * general interface for all search index manager
 * 
 * @author dzo
 */
public interface SearchIndexManager {
	
	/**
	 * delete the specified index
	 * @param indexName
	 */
	public void deleteIndex(final String indexName);
	
	/**
	 * update the index
	 */
	public void updateIndex();
	
	/**
	 * @return index information about all managed search indices
	 */
	public List<SearchIndexInfo> getIndexInformations();
	
	/**
	 * regenerate the specified index
	 * @param indexName
	 * @throws IndexAlreadyGeneratingException
	 */
	public void regenerateIndex(final String indexName) throws IndexAlreadyGeneratingException;
	
	/**
	 * regenerate all indices
	 */
	public void regenerateAllIndices();
	
	/**
	 * 
	 * @throws IndexAlreadyGeneratingException
	 */
	public void generateIndex() throws IndexAlreadyGeneratingException;

	/**
	 * @param indexId
	 */
	public void enableIndex(String indexId);

}

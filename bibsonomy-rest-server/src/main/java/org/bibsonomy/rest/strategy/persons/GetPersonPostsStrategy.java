/**
 * BibSonomy-Rest-Server - The REST-server.
 *
 * Copyright (C) 2006 - 2021 Data Science Chair,
 *                               University of Würzburg, Germany
 *                               https://www.informatik.uni-wuerzburg.de/datascience/home/
 *                           Information Processing and Analytics Group,
 *                               Humboldt-Universität zu Berlin, Germany
 *                               https://www.ibi.hu-berlin.de/en/research/Information-processing/
 *                           Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               https://www.kde.cs.uni-kassel.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               https://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.rest.strategy.persons;

import static org.bibsonomy.rest.strategy.persons.GetListOfPersonsStrategy.extractAdditionalKey;
import static org.bibsonomy.util.ValidationUtils.present;

import java.io.Writer;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Person;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.ResourcePersonRelation;
import org.bibsonomy.model.User;
import org.bibsonomy.model.enums.PersonIdType;
import org.bibsonomy.model.enums.PersonPostsStyle;
import org.bibsonomy.model.enums.PersonResourceRelationOrder;
import org.bibsonomy.model.enums.PersonResourceRelationType;
import org.bibsonomy.model.extra.AdditionalKey;
import org.bibsonomy.model.logic.querybuilder.PostQueryBuilder;
import org.bibsonomy.model.logic.querybuilder.ResourcePersonRelationQueryBuilder;
import org.bibsonomy.rest.RESTConfig;
import org.bibsonomy.rest.exceptions.NoSuchResourceException;
import org.bibsonomy.rest.strategy.AbstractGetListStrategy;
import org.bibsonomy.rest.strategy.Context;
import org.bibsonomy.util.Sets;
import org.bibsonomy.util.UrlBuilder;


/**
 * FIXME: move the duplicate code to the code (see webapp)
 *
 * Strategy to get the publications of a person by their ID.
 *
 * @author kchoong
 */
public class GetPersonPostsStrategy extends AbstractGetListStrategy<List<? extends Post<? extends Resource>>> {

	private final String personId;
	private final AdditionalKey additionalKey;

	// TODO REMOVE ASAP
	public static final Set<PersonResourceRelationType> PUBLICATION_RELATED_RELATION_TYPES = Sets.asSet(PersonResourceRelationType.AUTHOR, PersonResourceRelationType.EDITOR);

	/**
	 * constructor for /posts/person
	 * @param context
	 */
	public GetPersonPostsStrategy(final Context context) {
		super(context);
		this.personId = context.getStringAttribute(RESTConfig.PERSON_ID_PARAM, null);
		this.additionalKey = extractAdditionalKey(context);
	}

	/**
	 * @param context
	 * @param personId
	 */
	public GetPersonPostsStrategy(final Context context, final String personId) {
		super(context);
		this.personId = personId;
		this.additionalKey = null;
	}

	@Override
	protected void render(final Writer writer, final List<? extends Post<? extends Resource>> resultList) {
		this.getRenderer().serializePosts(writer, resultList, this.getView());
	}

	private Person getPerson() {
		if (present(this.personId)) {
			return this.getLogic().getPersonById(PersonIdType.PERSON_ID, this.personId);
		}

		if (present(this.additionalKey)) {
			return this.getLogic().getPersonByAdditionalKey(this.additionalKey.getKeyName(), this.additionalKey.getKeyValue());
		}

		return null;
	}

	@Override
	protected List<? extends Post<? extends Resource>> getList() {
		final Person person = this.getPerson();
		if (!present(person)) {
			throw new NoSuchResourceException("person not found");
		}

		// check, if a user has claimed this person
		if (present(person.getUser())) {
			// Get person posts style settings of the linked user
			final User user = this.getAdminLogic().getUserDetails(person.getUser());
			final PersonPostsStyle personPostsStyle = user.getSettings().getPersonPostsStyle();

			if (personPostsStyle == PersonPostsStyle.MYOWN) {
				// Get 'myown' posts of the linked user
				final PostQueryBuilder myOwnqueryBuilder = new PostQueryBuilder()
								.fromTo(this.getView().getStartValue(), this.getView().getEndValue())
								.setGrouping(GroupingEntity.USER)
								.setGroupingName(person.getUser())
								.setTags(Collections.singletonList("myown")); // TODO: use the myown system tag

				return this.getLogic().getPosts(myOwnqueryBuilder.createPostQuery(BibTex.class));
			}
		}

		// Default: return the gold standards
		// TODO: this needs to be removed/refactored as soon as the ResourcePersonRelationQuery.ResourcePersonRelationQueryBuilder accepts start/end
		final ResourcePersonRelationQueryBuilder queryBuilder = new ResourcePersonRelationQueryBuilder()
						.byPersonId(person.getPersonId())
						.withPosts(true)
						.withPersonsOfPosts(true)
						.groupByInterhash(true)
						.orderBy(PersonResourceRelationOrder.PublicationYear)
						.fromTo(this.getView().getStartValue(), this.getView().getEndValue());

		final List<ResourcePersonRelation> resourceRelations = this.getLogic().getResourceRelations(queryBuilder.build());
		final List<Post<? extends BibTex>> otherAuthorRelations = new LinkedList<>();

		for (final ResourcePersonRelation resourcePersonRelation : resourceRelations) {
			final Post<? extends BibTex> post = resourcePersonRelation.getPost();
			final BibTex publication = post.getResource();
			final boolean isThesis = publication.getEntrytype().toLowerCase().endsWith("thesis");
			final boolean isAuthorEditorRelation = PUBLICATION_RELATED_RELATION_TYPES.contains(resourcePersonRelation.getRelationType());

			if (isAuthorEditorRelation) {
				if (!isThesis) {
					otherAuthorRelations.add(resourcePersonRelation.getPost());
				}
			}

			// we explicitly do not want ratings on the person pages because this might cause users of the genealogy feature to hesitate putting in their dissertations
			publication.setRating(null);
			publication.setNumberOfRatings(null);
		}

		return otherAuthorRelations;
	}

	@Override
	protected UrlBuilder getLinkPrefix() {
		if (present(this.additionalKey)) {
			return this.getUrlRenderer().createUrlBuilderForPersonPostsByAdditionalKey(this.additionalKey);
		}
		return this.getUrlRenderer().createUrlBuilderForPersonPosts(this.personId);
	}

}

/**
 * BibSonomy-OpenAccess - Check Open Access Policies for Publications
 *
 * Copyright (C) 2006 - 2021 Data Science Chair,
 *                               University of Würzburg, Germany
 *                               https://www.informatik.uni-wuerzburg.de/datascience/home/
 *                           Information Processing and Analytics Group,
 *                               Humboldt-Universität zu Berlin, Germany
 *                               https://www.ibi.hu-berlin.de/en/research/Information-processing/
 *                           Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               https://www.kde.cs.uni-kassel.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               https://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.unikassel.puma.openaccess.sherparomeo.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "funder",
    "publishercomplies",
    "compliancetype",
    "selectedtitles"
})
@XmlRootElement(name = "mandate")
public class Mandate {

    @XmlElement(required = true)
    protected Funder funder;
    @XmlElement(required = true)
    protected String publishercomplies;
    @XmlElement(required = true)
    protected String compliancetype;
    protected List<Selectedtitles> selectedtitles;

    /**
     * Gets the value of the funder property.
     * 
     * @return
     *     possible object is
     *     {@link Funder }
     *     
     */
    public Funder getFunder() {
        return funder;
    }

    /**
     * Sets the value of the funder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Funder }
     *     
     */
    public void setFunder(Funder value) {
        this.funder = value;
    }

    /**
     * Gets the value of the publishercomplies property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPublishercomplies() {
        return publishercomplies;
    }

    /**
     * Sets the value of the publishercomplies property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPublishercomplies(String value) {
        this.publishercomplies = value;
    }

    /**
     * Gets the value of the compliancetype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompliancetype() {
        return compliancetype;
    }

    /**
     * Sets the value of the compliancetype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompliancetype(String value) {
        this.compliancetype = value;
    }

    /**
     * Gets the value of the selectedtitles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectedtitles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectedtitles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Selectedtitles }
     * 
     * 
     */
    public List<Selectedtitles> getSelectedtitles() {
        if (selectedtitles == null) {
            selectedtitles = new ArrayList<Selectedtitles>();
        }
        return this.selectedtitles;
    }

}

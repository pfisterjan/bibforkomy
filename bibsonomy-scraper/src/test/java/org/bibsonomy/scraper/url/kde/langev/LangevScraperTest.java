/**
 * BibSonomy-Scraper - Web page scrapers returning BibTeX for BibSonomy.
 *
 * Copyright (C) 2006 - 2021 Data Science Chair,
 *                               University of Würzburg, Germany
 *                               https://www.informatik.uni-wuerzburg.de/datascience/home/
 *                           Information Processing and Analytics Group,
 *                               Humboldt-Universität zu Berlin, Germany
 *                               https://www.ibi.hu-berlin.de/en/research/Information-processing/
 *                           Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               https://www.kde.cs.uni-kassel.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               https://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.scraper.url.kde.langev;

import static org.bibsonomy.scraper.junit.RemoteTestAssert.assertScraperResult;

import org.bibsonomy.scraper.junit.RemoteTest;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Scraper URL tests #86 & #87 for ISRLScraper
 * @author wbi
 */
@Category(RemoteTest.class)
public class LangevScraperTest {
	
	/**
	 * starts URL test with id url_86
	 */
	@Test
	@Ignore  // * this scraper is now disabled because the website http://www.isrl.uiuc.edu/ is no more availabl
	public void urlTestRun1(){
		assertScraperResult("http://www.isrl.uiuc.edu/~amag/langev/paper/mitchener07middleEnglish.html", LangevScraper.class, "LangevScraperUnitURLTest1.bib");
	}
	
	/**
	 * starts URL test with id url_87
	 */
	@Test
	@Ignore // * this scraper is now disabled because the website http://www.isrl.uiuc.edu/ is no more availabl
	public void urlTestRun2(){
		assertScraperResult("http://www.isrl.uiuc.edu/~amag/langev/paper/arbib06mirrorSystemEditedBook.html", LangevScraper.class, "LangevScraperUnitURLTest2.bib");
	}
}

/**
 * BibSonomy-Scraper - Web page scrapers returning BibTeX for BibSonomy.
 *
 * Copyright (C) 2006 - 2021 Data Science Chair,
 *                               University of Würzburg, Germany
 *                               https://www.informatik.uni-wuerzburg.de/datascience/home/
 *                           Information Processing and Analytics Group,
 *                               Humboldt-Universität zu Berlin, Germany
 *                               https://www.ibi.hu-berlin.de/en/research/Information-processing/
 *                           Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               https://www.kde.cs.uni-kassel.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               https://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.scraper.url.kde.journalogy;

import static org.bibsonomy.scraper.junit.RemoteTestAssert.assertScraperResult;

import org.bibsonomy.scraper.junit.RemoteTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Scraper URL tests #193 & #194
 * @author clemens
 */
@Category(RemoteTest.class)
public class JournalogyScraperTest {
	/**
	 * starts URL test with id url_193
	 */
	@Test
	public void url1TestRun(){
		assertScraperResult("http://academic.research.microsoft.com/Paper/13850916", JournalogyScraper.class, "JournalogyScraperUnitURLTest1.bib");
	}
	
	/**
	 * starts URL test with id url_194
	 */
	@Test
	public void url2TestRun(){
		assertScraperResult("http://www.journalogy.org/Paper/372540", JournalogyScraper.class, "JournalogyScraperUnitURLTest2.bib");
	}
	
	/**
	 * starts URL test with id url_195
	 */
	@Test
	public void url3TestRun(){
		assertScraperResult("http://academic.research.microsoft.com/Publication/16088645/", JournalogyScraper.class, "JournalogyScraperUnitURLTest3.bib");
	}
}

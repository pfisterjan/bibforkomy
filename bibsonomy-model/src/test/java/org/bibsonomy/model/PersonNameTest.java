/**
 * BibSonomy-Model - Java- and JAXB-Model.
 *
 * Copyright (C) 2006 - 2021 Data Science Chair,
 *                               University of Würzburg, Germany
 *                               https://www.informatik.uni-wuerzburg.de/datascience/home/
 *                           Information Processing and Analytics Group,
 *                               Humboldt-Universität zu Berlin, Germany
 *                               https://www.ibi.hu-berlin.de/en/research/Information-processing/
 *                           Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               https://www.kde.cs.uni-kassel.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               https://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.model;

import static org.junit.Assert.assertEquals;

import org.bibsonomy.model.util.PersonNameUtils;
import org.junit.Test;

/**
 * @author rja
 */
public class PersonNameTest {

	/**
	 * 
	 */
	@Test
	public void testEqualsAndHashCodeWhiteSpace() {
		final PersonName p1 = new PersonName(null, "Knuth");
		final PersonName p2 = new PersonName(" ", "Knuth");
		final PersonName p3 = new PersonName("", "Knuth");
		final PersonName p4 = new PersonName("\n", "Knuth");
		assertEquals(p1, p1);
		assertEquals(p1, p2);
		assertEquals(p1, p3);
		assertEquals(p1, p4);
		assertEquals(p2, p1);
		assertEquals(p2, p2);
		assertEquals(p2, p3);
		assertEquals(p2, p4);
		assertEquals(p3, p1);
		assertEquals(p3, p2);
		assertEquals(p3, p3);
		assertEquals(p3, p4);
		assertEquals(p4, p1);
		assertEquals(p4, p2);
		assertEquals(p4, p3);
		assertEquals(p4, p4);
		assertEquals(p1.hashCode(), p1.hashCode());
		assertEquals(p1.hashCode(), p2.hashCode());
		assertEquals(p1.hashCode(), p3.hashCode());
		assertEquals(p1.hashCode(), p4.hashCode());
		assertEquals(p2.hashCode(), p1.hashCode());
		assertEquals(p2.hashCode(), p2.hashCode());
		assertEquals(p2.hashCode(), p3.hashCode());
		assertEquals(p2.hashCode(), p4.hashCode());
		assertEquals(p3.hashCode(), p1.hashCode());
		assertEquals(p3.hashCode(), p2.hashCode());
		assertEquals(p3.hashCode(), p3.hashCode());
		assertEquals(p3.hashCode(), p4.hashCode());
		assertEquals(p4.hashCode(), p1.hashCode());
		assertEquals(p4.hashCode(), p2.hashCode());
		assertEquals(p4.hashCode(), p3.hashCode());
		assertEquals(p4.hashCode(), p4.hashCode());
	}
	
	@Test
	public void testStrangeBehaviour1() throws Exception {
		final PersonName p1 = PersonNameUtils.discoverPersonNames("Lonely Writer").get(0);
	}

}

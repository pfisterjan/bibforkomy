/**
 * BibSonomy-Model - Java- and JAXB-Model.
 *
 * Copyright (C) 2006 - 2021 Data Science Chair,
 *                               University of Würzburg, Germany
 *                               https://www.informatik.uni-wuerzburg.de/datascience/home/
 *                           Information Processing and Analytics Group,
 *                               Humboldt-Universität zu Berlin, Germany
 *                               https://www.ibi.hu-berlin.de/en/research/Information-processing/
 *                           Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               https://www.kde.cs.uni-kassel.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               https://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.model.extra;

import java.io.Serializable;
import java.net.URL;
import java.util.Date;

/**
 * Holds additional information about BibTexs.
 * 
 * @author Christian Schenk
 */
public class BibTexExtra implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4004329258093393977L;
	private URL url;
	private String text;
	private Date date;

	/**
	 * default constructor
	 */
	public BibTexExtra() {
		// noop
	}

	/**
	 * Constructor setting all three fields.
	 * @param url
	 * @param text
	 * @param date
	 */
	public BibTexExtra(URL url, String text, Date date) {
		super();
		this.url = url;
		this.text = text;
		this.date = date;
	}

	/**
	 * @return date
	 */
	public Date getDate() {
		return this.date;
	}

	/**
	 * @param date
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return text
	 */
	public String getText() {
		return this.text;
	}

	/**
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return url
	 */
	public URL getUrl() {
		return this.url;
	}

	/**
	 * @param url
	 */
	public void setUrl(URL url) {
		this.url = url;
	}
}
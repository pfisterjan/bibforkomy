/**
 * BibSonomy-Model - Java- and JAXB-Model.
 *
 * Copyright (C) 2006 - 2021 Data Science Chair,
 *                               University of Würzburg, Germany
 *                               https://www.informatik.uni-wuerzburg.de/datascience/home/
 *                           Information Processing and Analytics Group,
 *                               Humboldt-Universität zu Berlin, Germany
 *                               https://www.ibi.hu-berlin.de/en/research/Information-processing/
 *                           Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               https://www.kde.cs.uni-kassel.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               https://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.model;

import java.util.Date;

import org.bibsonomy.model.enums.PersonResourceRelationType;

/**
 * TODO: add documentation to this class
 *
 * @author Chris
 */
// TODO rename to PersonResourceRelation
public abstract class ResourcePersonRelationBase {
	private int personRelChangeId;
	private PersonResourceRelationType relationType;
	private int qualifying;
	/** name of the person who created this link */
	private String changedBy;
	private Date changedAt;

	/** the position in the resource's list of authors / editors / ... */
	private int personIndex;
	
	/**
	 * @return the id
	 */
	public int getPersonRelChangeId() {
		return this.personRelChangeId;
	}
	/**
	 * @param id the id to set
	 */
	public void setPersonRelChangeId(int id) {
		this.personRelChangeId = id;
	}

	/**
	 * @return the qualifying
	 */
	public int getQualifying() {
		return this.qualifying;
	}
	/**
	 * @param qualifying the qualifying to set
	 */
	public void setQualifying(int qualifying) {
		this.qualifying = qualifying;
	}

	/**
	 * @return the authorIndex
	 */
	public int getPersonIndex() {
		return this.personIndex;
	}
	/**
	 * @param authorIndex the authorIndex to set
	 */
	public void setPersonIndex(int authorIndex) {
		this.personIndex = authorIndex;
	}
	public PersonResourceRelationType getRelationType() {
		return this.relationType;
	}
	public void setRelationType(PersonResourceRelationType relationType) {
		this.relationType = relationType;
	}
	public String getChangedBy() {
		return this.changedBy;
	}
	public void setChangedBy(String createdByUserName) {
		this.changedBy = createdByUserName;
	}
	public Date getChangedAt() {
		return this.changedAt;
	}
	public void setChangedAt(Date changedAt) {
		this.changedAt = changedAt;
	}
	
}

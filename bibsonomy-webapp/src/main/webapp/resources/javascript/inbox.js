/**
 * Initiate confirmation dialog for clear inbox button
 */
$(document).ready(function() {
	$(".clear-inbox-confirmation").confirmation({
		"placement": "bottom"
	});
});
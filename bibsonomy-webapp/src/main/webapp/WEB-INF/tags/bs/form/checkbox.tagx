<jsp:root version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/buttons"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
	xmlns:c="http://java.sun.com/jsp/jstl/core">

	<jsp:directive.tag description="Render a form input checkbox in bootstrap3 style." />

	<jsp:directive.attribute name="path" type="java.lang.String" required="true" description="Path to property for data binding (e.g., post.resource.journal), labels, and the help message (.help is appended)"/>
	<jsp:directive.attribute name="inputColSpan" type="java.lang.Integer" required="false" description="Column width for this checkbox and its label." />
	
	
	<jsp:directive.attribute name="labelText" type="java.lang.String" required="false" description="Alternativly to path, you can define directly a description for the label."/>
	<jsp:directive.attribute name="labelKey" type="java.lang.String" required="false" description="Alternativly to path, you can define directly the message key for the label."/>
	<jsp:directive.attribute name="helpText" type="java.lang.String" required="false" description="Alternativly to path.help, you can define the content for popover help" />
	<jsp:directive.attribute name="helpKey" type="java.lang.String" required="false" description="Alternativly to path.help, you can define the message key for popover help" />
	<jsp:directive.attribute name="noHelp" type="java.lang.Boolean" required="false" description="Shall the fly-by help be shown?"/>
	
	<!-- HTML attributes -->
	<jsp:directive.attribute name="id" type="java.lang.String" required="false" description="Id for input field"/>
	<jsp:directive.attribute name="tabindex" type="java.lang.Integer" required="false" description="tabindex for input field"/>
	<jsp:directive.attribute name="name" type="java.lang.String" required="false" description="name attribute for input field"/>
	<jsp:directive.attribute name="disabled" type="java.lang.Boolean" required="false" description="element disabled"/>
	<jsp:directive.attribute name="classAtt" type="java.lang.String" required="false" description="classes for the checkbox"/>

	<c:if test="${ empty inputColSpan or inputColSpan lt 2}">
		<c:set var="inputColSpan" value="10" />
	</c:if>
	
	<c:if test="${empty labelText and empty labelKey}">
		<fmt:message var="labelText" key="${path}" />
	</c:if>
	
	<c:if test="${not empty labelKey}">
		<fmt:message var="labelText" key="${labelKey}" />
	</c:if>
	
	<c:if test="${empty helpText and empty helpKey}">
		<fmt:message var="helpText" key="${path}.help" />
	</c:if>
	
	<c:if test="${not empty helpKey}">
		<fmt:message var="helpText" key="${helpKey}" />
	</c:if>
	
	<c:set var="formErrors"><form:errors path="${path}" /></c:set>
	
	<div class="form-group${not empty formErrors ? ' has-error' : ''}">
		<div class="col-sm-offset-${12-inputColSpan} col-sm-${inputColSpan}">
			<c:set var="classValue" value="${not empty noHelp or not noHelp ? 'help-popover' : ''}"/>
			<c:if test="${not empty classAtt}">
				<c:set var="classValue" value="${classValue} ${classAtt}"/>
			</c:if>
			<div class="checkbox">
				<label>
					<c:choose>
						<c:when test="${not empty id }">
							<form:checkbox
								id="${id}"
								path="${path}" 
								class="${classValue}" 
								tabindex="${tabindex}"
								value="${false}"
								disabled="${disabled}"
							/>
						</c:when>
						<c:otherwise>
							<form:checkbox
								path="${path}" 
								class="${classValue}" 
								tabindex="${tabindex}"
								value="${false}"
								disabled="${disabled}"
							/>
						</c:otherwise>
					</c:choose>
					
					 <c:out value="${labelText}"/>
					<c:if test="${empty noHelp or not noHelp}">
						<div class="help-content hide">${helpText}</div>
					</c:if>
					<bsform:errors path="${path}" />
				</label>
			</div>
		</div>
	</div>

</jsp:root>
<jsp:root version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:c="http://java.sun.com/jsp/jstl/core">
	
	<jsp:directive.tag description="Renders a bootstrap3 button." />
	
	
	<jsp:directive.attribute type="java.lang.String" name="value" required="false" description="inscription of the button" />
	<jsp:directive.attribute type="java.lang.String" name="valueKey" required="false" description="inscription of the button" />
	<jsp:directive.attribute type="java.lang.String" name="id" required="false" description="id of the button" />
	<jsp:directive.attribute type="java.lang.String" name="href" required="false" description="link of the button" />
	<jsp:directive.attribute type="java.lang.String" name="title" required="false" description="title of the button" />
	<jsp:directive.attribute type="java.lang.String" name="titleKey" required="false" description="key for the title of the button" />
	<jsp:directive.attribute type="java.lang.String" name="srText" required="false" description="screen reader text" />
	<jsp:directive.attribute name="block" type="java.lang.Boolean" required="false" />
	<jsp:directive.attribute type="org.bibsonomy.webapp.view.constants.BootstrapButtonStyle" name="style" required="true" description="Button Style Type" />
	<jsp:directive.attribute type="org.bibsonomy.webapp.view.constants.BootstrapButtonSize" name="size" required="true" description="Button Style Type" />
	<jsp:directive.attribute type="java.lang.String" name="type" required="false" description="content for html attribute type" />
	<jsp:directive.attribute type="java.lang.String" name="name" required="false" description="name of the button" />
	<jsp:directive.attribute type="java.lang.String" name="icon" required="false" description="icon dispayed before value" />
	<jsp:directive.attribute type="java.lang.String" 	name="dataToggle" 	required="false" description="If you need to use attribute 'data-toggle'."/>
	<jsp:directive.attribute type="java.lang.String" 	name="dataTarget" 	required="false" description="If you need to use attribute 'data-target'."/>
	<jsp:directive.attribute type="java.lang.String" 	name="dataDismiss"	required="false" description="If you need to use attribute 'data-dismiss'." />
	<jsp:directive.attribute type="java.lang.Boolean"	name="dropdown"		required="false" description="Add dropdown menu to the button" />
	<jsp:directive.attribute type="java.lang.Boolean"	name="dropup"		required="false" description="Add dropdown menu to the button" />
	<jsp:directive.attribute type="java.lang.Boolean"	name="gear"			required="false" description="Add dropdown menu to the button" />
	<jsp:directive.attribute type="java.lang.String"	name="className"	required="false" description="Additional class for the button" />
	<jsp:directive.attribute type="java.lang.String"	name="disabled"		required="false" description="Should the button be disabled?"/>
	<jsp:directive.attribute type="java.lang.Boolean"	name="forceEnabled"		required="false" description="Should the button be fored active?"/>
	<jsp:directive.attribute type="java.lang.Boolean"	name="hideCaret"		required="false" description="Should the caret be hidden, even if it is a dropdown?"/>
	
	<jsp:directive.attribute name="onclick" type="java.lang.String" required="false" description="javascript code for execution" />
	<jsp:directive.attribute name="tabindex" type="java.lang.Integer" required="false" description="" />
	
	<c:if test="${not empty titleKey}">
		<fmt:message key="${titleKey}" var="title" />
	</c:if>
	
	<c:if test="${not empty valueKey}">
		<fmt:message key="${valueKey}" var="value" />
	</c:if>
	
	<c:if test="${empty type}">
		<c:set var="type" value="button" />
	</c:if>
	
	<c:if test="${not empty className}">
		<c:set var="className" value="${fn:escapeXml(className)} " />
	</c:if>
	
	<c:if test="${(not empty disabled and disabled) or (systemReadOnly and empty forceEnabled)}">
		<c:set var="className" value="${className}disabled " />
	</c:if>
	
	<c:if test="${not empty block and block}">
		<c:set var="className" value="${className}btn-block " />
	</c:if>
	
	<c:if test="${empty hideCaret}">
		<c:set var="hideCaret" value="${false}" />
	</c:if>
	
	<button title="${fn:escapeXml(title)}" id="${id}" type="${fn:escapeXml(type)}" class="${className}btn ${fn:escapeXml(style.cssClassName)} ${fn:escapeXml(size.cssClassName)} ${dropdown ? 'dropdown-toggle':''} ${dropup ? 'dropup-toggle':''}" data-toggle="${fn:escapeXml(dataToggle)}" data-target="${fn:escapeXml(dataTarget)}" data-dismiss="${fn:escapeXml(dataDismiss)}" onclick="${fn:escapeXml(onclick)}" name="${name}" value="${value}">
		<c:if test="${not empty icon}">
			<span class="fa fa-${icon}"><!-- keep me --></span><c:out value=" " />
			<c:if test="${not empty srText}">
				<span class="sr-only"><c:out value="${srText}" /></span>
			</c:if>
		</c:if>
		<c:if test="${not empty value and not gear}">
			<span class="button-text"><c:out value="${value}" /></span>
		</c:if>
		<c:if test="${gear}">
			&amp;nbsp;<span class="fa fa-gear"><!-- KEEP ME --></span>&amp;nbsp;
			<span class="sr-only">edit</span> <!-- TODO: i18n for edit -->
		</c:if>
		<c:if test="${(dropdown or dropup) and not hideCaret}">
			&amp;nbsp;<span class="caret"><!-- KEEP ME --></span>
		</c:if>
	</button>
</jsp:root>
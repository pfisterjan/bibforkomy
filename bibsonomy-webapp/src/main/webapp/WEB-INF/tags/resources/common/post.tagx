<jsp:root version="2.0" 
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:rc="urn:jsptagdir:/WEB-INF/tags/resources/common"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:discussion ="urn:jsptagdir:/WEB-INF/tags/resources/discussion"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
	xmlns:bs="urn:jsptagdir:/WEB-INF/tags/layout/bootstrap"
	xmlns:post="urn:jsptagdir:/WEB-INF/tags/post"
	xmlns:form="http://www.springframework.org/tags/form">
	
	<jsp:directive.attribute name="loginUserName" type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="otherPostsUrlPrefix" type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="post" type="org.bibsonomy.model.Post" required="true"/>
	<jsp:directive.attribute name="additionalItemClasses" type="java.lang.String" required="false"/>
	<jsp:directive.attribute name="resourceType" type="java.lang.String" required="true"/>

	<jsp:directive.attribute name="hideMetaData" type="java.lang.Boolean" required="false" />
	<jsp:directive.attribute name="hideCounts" type="java.lang.Boolean" required="false"/>
	<jsp:directive.attribute name="hidePostMenu" type="java.lang.Boolean" required="false" />
	<jsp:directive.attribute name="headlineInBody" type="java.lang.Boolean" required="false" />

	<jsp:directive.attribute name="title" fragment="true" required="true"/>
	<jsp:directive.attribute name="desc" fragment="true" required="true"/>
	<jsp:directive.attribute name="actions" fragment="true" required="true"/>
	<jsp:directive.attribute name="documents" fragment="true" required="true"/>

	<c:if test="${empty hideMetaData}">
		<c:set var="hideMetaData" value="${false}" />
	</c:if>

	<c:if test="${empty headlineInBody}">
		<c:set var="headlineInBody" value="${false}" />
	</c:if>

	<c:if test="${empty hideCounts}">
		<c:set var="hideCounts" value="${false}" />
	</c:if>

	<c:if test="${empty hidePostMenu}">
		<c:set var="hidePostMenu" value="${false}" />
	</c:if>

	<c:set var="isRemotePost" value="${not empty post.systemUrl and not mtl:isSameHost(post.systemUrl, properties['project.home'])}" />
	
	<!-- FIXME: not nice here-->
	<c:choose>
		<c:when test="${resourceType eq 'bibtex'}">
			<c:choose>
				<c:when test="${post.resource.entrytype eq 'book'}">
					<c:set var="itemtype" value="http://schema.org/Book" />
				</c:when>
				<c:otherwise>
					<c:set var="itemtype" value="http://schema.org/ScholarlyArticle" />
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:otherwise>
			<c:set var="itemtype" value="http://schema.org/WebSite" />
		</c:otherwise>
	</c:choose>
	
	<c:set var="editableByUser" value="${loginUserName eq post.user.name}" />
	<c:set var="loginUser" value="${command.context.loginUser}" />
	<c:forEach items="${loginUser.groups}" var="group">
		<c:if test="${group.name eq post.user.name }">
			<c:set var="ms" value="${group.getGroupMembershipForUser(loginUser.name) }" />
			<c:if test="${not empty ms and (ms.groupRole eq 'MODERATOR' or ms.groupRole eq 'ADMINISTRATOR') }">
				<c:set var="editableByUser" value="${true}" />
			</c:if>
		</c:if>
	</c:forEach>
	
	
   <!--+ 
       | TODO: Several tests (not empty post.user.name) are here to test for community posts. 
       | They never have a userName. 
       | Since they also dont have several other properties, we do not show those.
       +-->
	<!-- FIXME: replace 2 with constant! -->
	<li data-intrahash="2${post.resource.intraHash}" itemtype="${itemtype}" itemscope="itemscope" data-user="${fn:escapeXml(post.user.name)}" class="media post ${additionalItemClasses}" id="list-item-${post.resource.intraHash}${post.user.name}">
		<div class="clearline">&#160;</div>
		<c:if test="${!headlineInBody}">
			<h4 class="media-heading" data-ident="${post.resource.intraHash}">
				<div>
				<div class="clearline">&#160;</div>
				<c:if test="${not hideCounts}">
					<rc:otherpeople post="${post}" />
				</c:if>
				<span class="ptitle"><jsp:invoke fragment="title"/></span>
				<div class="clearline">&#160;</div>
				</div>
			</h4>
		</c:if>
		<c:if test="${not hideMetaData}">
			<div class="hidden-xs hidden-md media-left thumbnail-container">

				<jsp:invoke fragment="documents"/>
				<!-- IN MY COLLECTION: only display the in-my-collection-icon when the user is logged in -->
				<c:choose>
					<c:when test="${isRemotePost}">
						<c:set var="extraCssClass" value="remote-post fa-external-link"/>
					</c:when>
					<c:when test="${command.context.userLoggedIn and not empty post.user.name and (loginUserName eq post.user.name)}">
						<c:set var="extraCssClass" value="in-my-collection fa-bookmark"/>
					</c:when>
					<c:otherwise>
						<c:set var="extraCssClass" value="in-my-collection"/>
					</c:otherwise>
				</c:choose>
				<div class="fa ${extraCssClass}"><!--  --></div>


				<c:forEach var="group" items="${post.groups}">
					<c:if test="${!empty group.name and group.name ne 'public'}">
						<c:choose>
							<c:when test="${group.name eq 'friends'}">
								<!-- TODO: friends marker? -->
							</c:when>
							<c:when test="${group.name eq 'private'}">
								<div class="fa fa-eye-slash"><!--  --></div>
							</c:when>
						</c:choose>
					</c:if>
				</c:forEach>
			</div>
		</c:if>
		<div class="media-body">
			<c:if test="${headlineInBody}">
				<h4 class="media-heading" data-ident="${post.resource.intraHash}">
					<div>
						<div class="clearline">&#160;</div>
						<c:if test="${not hideCounts}">
							<rc:otherpeople post="${post}" />
						</c:if>
						<span class="ptitle"><jsp:invoke fragment="title"/></span>
						<div class="clearline">&#160;</div>
					</div>
				</h4>
			</c:if>

			<div class="pdesc ellipsis">
				<div class="ellipsis-content"><jsp:invoke fragment="desc"/></div>
			</div>
			<c:if test="${not hideMetaData}">
				<div class="pmeta">
					<rc:metaDate post="${post}"/><rc:metaUsername post="${post}" /><rc:metaGroups post="${post}" loginUserName="${loginUserName}" />
				</div>
			</c:if>
			<c:if test="${not empty post.tags}">
				<div class="ptags">
					<!-- Edit tags link -->
					<!-- TODO: Enable tag edits for group admins/mods -->
					<c:if test="${command.context.userLoggedIn and not empty post.user.name and editableByUser and not isRemotePost }">
						<fmt:message key="navi.editTags" var="editTags" />
						<bs:linkButton icon="pencil" dataToggle="modal" dataTarget="#tag-modal-${post.resource.intraHash}${post.user.name}" title="${editTags}" href="#" size="xsmall" additionalClass="tags-edit-link"></bs:linkButton>
						&amp;nbsp;
					</c:if>
	<span class="all-tags-button label label-tag pull-right hidden">
					<a href="#" class="tag-popover">
						<fmt:message key="post.actions.showAllTags" />
					</a>
					<div class="popover-content-custom hidden">
						<button type="button" class="close popover-content-custom-close" onclick="javascript:$(this).parent().parent().prev().popover('hide');">
							&amp;nbsp;
							<span aria-hidden="true">&amp;times;</span>
							<span class="sr-only">Close</span>
						</button>
						<rc:metaTags post="${post}" renderAsList="true" />
					</div>
				</span>				<rc:metaTags post="${post}" showTagsLabel="${showTagsLabel}"/>
				</div>
			</c:if>
			<div class="post-rating pull-left">
				<discussion:rating post="${post}" />
			</div>
			<c:if test="${not hidePostMenu}">
				<div class="post-buttons pull-right">
					<jsp:invoke fragment="actions"/>
				</div>
			</c:if>
			<div class="clearfix">&#160;</div>
			
			<!-- EDIT TAGS MODAL POPUP-->
			<!-- TODO: Enable tag edits for group admins/mods -->
			<c:if test="${command.context.userLoggedIn and not empty post.user.name and editableByUser}">
				
				<fmt:message key="tag.addTag" var="addTag" />
			
				<div id="tag-modal-${post.resource.intraHash}${post.user.name}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<c:url var="batchEditUrl" value="/batchEdit">
							<c:param name="deleteCheckedPosts" value="true" />
							<c:param name="format" value="action" />
							<c:param name="resourcetype" value="${resourceType}" />
							<c:param name="action" value="2" />
							<c:param name="updateExistingPost" value="true"/>
						</c:url>
						
						<form data-resource-hash="${post.resource.intraHash}${post.user.name}" class="form-horizontal edit-tags-form" method="post" action="${batchEditUrl}">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span aria-hidden="true">&amp;times;</span><span class="sr-only">Close</span>
									</button>
									<h4 class="modal-title"><c:out value="${editTags}" /></h4>
								</div>
								<div class="modal-body">
									<div class="response-msg"><!--  --></div>
									<div class="form-group">
										<label class="col-sm-3"><fmt:message key="tags" /></label>
										<div class="col-sm-9">
											<c:set var="tagString" value=" " />
											<c:forEach var="tag" items="${post.tags}">
												<c:set var="tagString" value="${tagString} ${tag.name}" />
											</c:forEach>
											<input type="hidden" name="ckey" value="${command.context.ckey }"/>
											<input type="hidden" name="posts['${post.resource.intraHash}_${post.user.name}'].oldTags" value="${fn:escapeXml(fn:trim(tagString))}" />
											<input type="hidden" name="posts['${post.resource.intraHash}_${post.user.name}'].checked" value="true" checked="checked" /> 
											<input class="edit-tagsinput form-control" name="posts['${post.resource.intraHash}_${post.user.name}'].newTags" type="text" value="${fn:escapeXml(fn:trim(tagString))} ${systemTagElementStr}" />
										</div>
									</div>
									
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="close" /></button>
									<button type="submit" class="btn btn-primary"><fmt:message key="save" /></button>
								</div>
							</div>
							<!-- /.modal-content -->
						</form>
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
			</c:if>
			<!-- / EDIT TAGS MODAL POPUP-->
		</div>
		<div class="clearline">&#160;</div>
	</li>
</jsp:root>
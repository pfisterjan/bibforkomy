<jsp:root version="2.0" xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:user="urn:jsptagdir:/WEB-INF/tags/resources/user"
	xmlns:discussionItem="urn:jsptagdir:/WEB-INF/tags/resources/discussion/discussionItem">
	
	<jsp:directive.attribute name="discussionItem" type="org.bibsonomy.model.DiscussionItem" required="true" />
	<jsp:directive.attribute name="resource" type="org.bibsonomy.model.Resource" required="true" />
	<jsp:directive.attribute name="loginUser" type="org.bibsonomy.model.User" required="true" />
	<jsp:directive.attribute name="content" fragment="true" required="true"/>
	<jsp:directive.attribute name="editor" fragment="true" required="false"/>

	<c:set var="discussionItemUserName" value="${discussionItem.user.name}" />

	<c:set var="ownDiscussionItem" value="${loginUser.name eq discussionItemUserName}" />
	<c:set var="anonymous" value="${discussionItem.anonymous}" />

	<c:set var="clazz" value="${fn:toLowerCase(discussionItem['class'].simpleName)}" />
	
	<div class="media-left">
		<c:choose>
			<c:when test="${anonymous and not ownDiscussionItem}">
				<c:if test="${properties['profile.picture.default']}">
					<a>
						<img src="${resdir}/image/anonymous.png" alt="anonymous" class="media-object commentPicture img-circle" />
					</a>
				</c:if>
			</c:when>
			<c:otherwise>
				<user:thumbnail user="${discussionItem.user}" noname="${true}" circle="${true}" size="44" />
			</c:otherwise>
		</c:choose>
	</div>
	<c:set var="itemtype" value="http://schema.org/Review"/>
	<c:set var="itemprop" value="review"/>
	<c:if test="${clazz eq 'comment'}">
		<c:set var="itemtype" value="http://schema.org/Comment"/>
		<c:set var="itemprop" value="comment"/>
	</c:if>
	<div class="media-body" itemprop="${itemprop}" itemscope="itemscope" itemtype="${itemtype}">
		<div class="info" data-username="${discussionItem.user.name}" data-discussion-item-hash="${discussionItem.hash}">
			<discussionItem:discussionItemOwner discussionItem="${discussionItem}" loginUser="${loginUser}"/>
			<discussionItem:discussionItemInfo discussionItem="${discussionItem}" anonymous="${anonymous}" ownDiscussionItem="${ownDiscussionItem}" />
		</div>
		<div class="${clazz} details">
			<jsp:invoke fragment="content"/>
			<div class="citeBox" style="display:none" >
				<!-- keep it -->
				<span class="cite-header"><fmt:message key="discussion.references" /></span>
			</div>
			<div class="bookCiteBox" style="display:none">
				<!-- keep it -->
				<span class="cite-header"><fmt:message key="discussion.bookmarkReferences" /></span>
			</div>
		</div>
		
		<c:if test="${not empty editor}">
			<jsp:invoke fragment="editor"/>
		</c:if>
		
		<div class="actions">
			<discussionItem:discussionItemActions discussionItem="${discussionItem}" />
		</div>
		
		<!-- sub items -->
		<discussionItem:subDiscussionItems parentItem="${discussionItem}" loginUser="${loginUser}" resource="${resource}" hidden="${true}"/>
	</div>
</jsp:root>
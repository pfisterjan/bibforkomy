<?xml version="1.0" encoding="UTF-8" ?>

	<!-- a tag library descriptor -->

<taglib xmlns="http://java.sun.com/xml/ns/j2ee" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd" version="2.0">
	<tlibversion>1.1</tlibversion>
	<jspversion>1.1</jspversion>
	<shortname>mytaglib</shortname>
	<uri></uri>
	<info>Some helpers to shorten syntax</info>

	<tag>
		<name>cdata</name>
		<tagclass>tags.CData</tagclass>
		<bodycontent>tagdependent</bodycontent>
		<info>Writes an XML CDATA section around the given content</info>
	</tag>

	<tag>
		<name>debug</name>
		<tagclass>tags.Debug</tagclass>
		<bodycontent>empty</bodycontent>
		<info>Writes a debug message using the logger</info>
		<attribute>
			<name>message</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>exists</name>
		<info>Checks, if the given (command) property exists. Only when it exists, the body of the tag is executed.</info>
		<tagclass>tags.Exists</tagclass>
		<bodycontent>JSP</bodycontent>
		<attribute>
			<name>path</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>
	
	<tag>
		<name>newRow</name>
		<info>Inserts a new row</info>
		<tagclass>org.bibsonomy.webapp.util.tags.RowTag</tagclass>
		<bodycontent>JSP</bodycontent>
	</tag>
	
	<tag>
		<name>strShorten</name>
		<info>Shortens a string</info>
		<tagclass>org.bibsonomy.webapp.util.tags.StringShortenerTag</tagclass>
		<bodycontent>JSP</bodycontent>
		<attribute>
			<name>value</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>maxlen</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>
	
	<tag>
		<name>renderJabRefLayout</name>
		<info>renders the JabRef layout</info>
		<tagclass>org.bibsonomy.webapp.util.tags.JabrefLayoutRendererTag</tagclass>
		<bodycontent>JSP</bodycontent>
		<attribute>
			<name>layout</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>post</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>posts</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>
	
	<tag>
		<name>favouriteLayoutDisplayName</name>
		<info>outputs the fav layout display name</info>
		<tagclass>org.bibsonomy.webapp.util.tags.FavouriteLayoutsDisplayNameTag</tagclass>
		<bodycontent>JSP</bodycontent>
		<attribute>
			<name>favouriteLayout</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>
	
	<tag>
		<name>messageSourceKeys</name>
		<info>stores all keys of the messagesource into the specified var</info>
		<tagclass>org.bibsonomy.webapp.util.tags.MessageSourceKeysTag</tagclass>
		<bodycontent>empty</bodycontent>
		<attribute>
			<name>locale</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>var</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<tag>
		<name>formatTimeDiff</name>
		<info>formats the time difference between two dates</info>
		<tagclass>org.bibsonomy.webapp.util.tags.TimeDiffFormatterTag</tagclass>
		<bodycontent>empty</bodycontent>
		<attribute>
			<name>startDate</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>endDate</name>
			<required>false</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>
	
	<tag>
		<name>styleSheet</name>
		<info>renders the link to the less file or css file</info>
		<tagclass>org.bibsonomy.webapp.util.tags.StyleSheetTag</tagclass>
		<bodycontent>empty</bodycontent>
		<attribute>
			<name>path</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>
	
	<tag>
		<name>publicationReference</name>
		<info>Replaces wiki-style links with html links</info>
		<tagclass>org.bibsonomy.webapp.util.tags.PublicationReferenceTag</tagclass>
		<bodycontent>JSP</bodycontent>
		<attribute>
			<name>text</name>
			<required>true</required>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>

	<function>
		<name>renderHover</name>
		<function-class>org.bibsonomy.webapp.util.tags.BibTexListTitleHoverFormatter</function-class>
		<function-signature>String renderHover(org.bibsonomy.model.Post, java.util.Locale)</function-signature>
	</function>

	<function>
		<name>normalize</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String normalize(java.lang.String, java.lang.String)</function-signature>
	</function>

	<function>
		<name>trimWhiteSpace</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String trimWhiteSpace(java.lang.String)</function-signature>
	</function>
	
	<function>
		<name>markdownToHtml</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String markdownToHtml(java.lang.String)</function-signature>
	</function>

	<function>
		<name>encodePathSegment</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String encodePathSegment(java.lang.String)</function-signature>
	</function>

	<function>
		<name>toTagString</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String toTagString(java.util.Collection)</function-signature>
	</function>

	<function>
		<name>getPath</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String getPath(java.lang.String)</function-signature>
	</function>

	<function>
		<name>getLowerPath</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String getLowerPath(java.lang.String)</function-signature>
	</function>

	<function>
		<name>getQuery</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String getQuery(java.lang.String)</function-signature>
	</function>

	<function>
		<name>ch</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String ch(java.lang.String)</function-signature>
	</function>

	<function>
		<name>containsResourceClass</name>
		<function-class>tags.Functions</function-class>
		<function-signature>boolean containsResourceClass(java.util.Collection, java.lang.String)</function-signature>
	</function>

	<function>
		<name>computeTagFontsize</name>
		<function-class>tags.Functions</function-class>
		<function-signature>java.lang.Integer computeTagFontsize(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.String)</function-signature>
	</function>

	<function>
		<name>cleanUrl</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String cleanUrl(java.lang.String)</function-signature>
	</function>

	<function>
		<name>isUrl</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String isUrl(java.lang.String)</function-signature>
	</function>

	<function>
		<name>setParam</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String setParam(java.lang.String, java.lang.String, java.lang.String)</function-signature>
	</function>

	<function>
		<name>removeParam</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String removeParam(java.lang.String, java.lang.String)</function-signature>
	</function>

	<function>
		<name>cleanBibtex</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String cleanBibtex(java.lang.String)</function-signature>
	</function>

	<function>
		<name>predictionString</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String getPredictionString(java.lang.Integer)</function-signature>
	</function>

	<function>
		<name>isSpammer</name>
		<function-class>tags.Functions</function-class>
		<function-signature>Boolean isSpammer(java.lang.Integer)</function-signature>
	</function>

	<function>
		<name>compareString</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String compareString(java.lang.String, java.lang.String)</function-signature>
	</function>
	
	<function>
		<name>diffEntries</name>
		<function-class>tags.Functions</function-class>
		<function-signature>java.util.Map diffEntries(org.bibsonomy.model.Post, org.bibsonomy.model.Post)</function-signature>
	</function>

	<function>
		<name>isRegularGroup</name>
		<function-class>tags.Functions</function-class>
		<function-signature>Boolean isRegularGroup(org.bibsonomy.model.Group)</function-signature>
	</function>
	
	<function>
		<name>quoteJSON</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String quoteJSON(java.lang.String)</function-signature>
	</function>

	<function>
		<name>quoteJSONcleanBibTeX</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String quoteJSONcleanBibTeX(java.lang.String)</function-signature>
	</function>

	<function>
		<!-- maps BibTeX entrytypes to SWRC entrytypes  -->
		<name>getSWRCEntryType</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String getSWRCEntryType(java.lang.String)</function-signature>
	</function>
	<function>
		<!-- maps BibTeX entrytypes to RIS/EndNote entrytypes  -->
		<name>getRISEntryType</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String getRISEntryType(java.lang.String)</function-signature>
	</function>

	<function>
		<name>getBibTeXEntryTypes</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String[] getBibTeXEntryTypes()</function-signature>
	</function>


	<function>
		<name>getTagSize</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String getTagSize(java.lang.Integer, java.lang.Integer)</function-signature>
	</function>

	<function>
		<name>getHostName</name>
		<function-class>tags.Functions</function-class>
		<function-signature>String getHostName(java.lang.String)</function-signature>
	</function>

	<function>
		<name>isLinkToDocument</name>
		<function-class>tags.Functions</function-class>
		<function-signature>Boolean isLinkToDocument(java.lang.String)</function-signature>
	</function>


	<function>
		<name>authorFontSize</name>
		<function-class>tags.Functions</function-class>
		<function-signature>Double authorFontSize(org.bibsonomy.model.Author, java.lang.Integer)</function-signature>
	</function>

	<function>
		<name>shortPublicationDescription</name>
		<function-class>tags.Functions</function-class>
		<function-signature>java.lang.String shortPublicationDescription(org.bibsonomy.model.Post)</function-signature>
	</function>

	<function>
		<name>shorten</name>
		<function-class>tags.Functions</function-class>
		<function-signature>java.lang.String shorten(java.lang.String, java.lang.Integer)</function-signature>
	</function>

	<function>
		<name>toBibtexString</name>
		<function-class>tags.Functions</function-class>
		<function-signature>java.lang.String toBibtexString(org.bibsonomy.model.Post, java.lang.String, java.lang.Boolean, java.lang.Boolean)</function-signature>
	</function>

	<function>
		<name>toEndnoteString</name>
		<function-class>tags.Functions</function-class>
		<function-signature>java.lang.String toEndnoteString(org.bibsonomy.model.Post, java.lang.Boolean)</function-signature>
	</function>

	<function>
		<name>contains</name>
		<function-class>tags.Functions</function-class>
		<function-signature>boolean contains(java.util.Collection, java.lang.Object)</function-signature>
	</function>

	<function>
		<name>toggleUserSimilarity</name>
		<function-class>tags.Functions</function-class>
		<function-signature>java.lang.String toggleUserSimilarity(java.lang.String)</function-signature>
	</function>

	<function>
		<name>extractDOI</name>
		<function-class>tags.Functions</function-class>
		<function-signature>java.lang.String extractDOI(java.lang.String)</function-signature>
	</function>

	<function>
		<name>removeInvalidXmlChars</name>
		<function-class>tags.Functions</function-class>
		<function-signature>java.lang.String removeInvalidXmlChars(java.lang.String)</function-signature>
	</function>

	<function>
		<name>hasTagMyown</name>
		<function-class>tags.Functions</function-class>
		<function-signature>boolean hasTagMyown(org.bibsonomy.model.Post)</function-signature>
	</function>
	
	<function>
		<name>hasReportedSystemTag</name>
		<function-class>tags.Functions</function-class>
		<function-signature>boolean hasReportedSystemTag(java.util.Set, java.lang.String)</function-signature>
	</function>

	<function>
		<name>getDate</name>
		<function-class>tags.Functions</function-class>
		<function-signature>java.lang.String getDate(java.lang.String, java.lang.String, java.lang.String, java.util.Locale)</function-signature>
	</function>

	<function>
		<name>formatDateISO8601</name>
		<function-class>tags.Functions</function-class>
		<function-signature>java.lang.String formatDateISO8601(java.util.Date)</function-signature>
	</function>
	
	<function>
		<name>formatDateW3CDTF</name>
		<function-class>tags.Functions</function-class>
		<function-signature>java.lang.String formatDateW3CDTF(java.util.Date)</function-signature>
	</function>

	<function>
		<name>formatDateMemento</name>
		<function-class>tags.Functions</function-class>
		<function-signature>java.lang.String formatDateMemento(java.util.Date)</function-signature>
	</function>

	<function>
		<name>formatDateRFC1123</name>
		<function-class>tags.Functions</function-class>
		<function-signature>java.lang.String formatDateRFC1123(java.util.Date)</function-signature>
	</function>
	
	<function>
		<name>otherPeopleColor</name>
		<function-class>tags.Functions</function-class>
		<function-signature>int otherPeopleColor(int)</function-signature>
	</function>
	
	<function>
		<name>convertToEnum</name>
		<function-class>tags.Functions</function-class>
		<function-signature>Object convertToEnum(java.lang.String, java.lang.String)</function-signature>
	</function>
	
	<function>
		<name>userIsGroup</name>
		<function-class>tags.Functions</function-class>
		<function-signature>boolean userIsGroup(org.bibsonomy.model.User)</function-signature>
	</function>
	
	<function>
		<name>uniqueDiscussionUsers</name>
		<function-class>tags.Functions</function-class>
		<function-signature>java.util.List uniqueDiscussionUsers(java.util.List)</function-signature>
	</function>
	
	<function>
		<name>isSameHost</name>
		<function-class>tags.Functions</function-class>
		<function-signature>boolean isSameHost(java.lang.String, java.lang.String)</function-signature>
	</function>

	<function>
		<name>filterGroups</name>
		<function-class>tags.Functions</function-class>
		<function-signature>java.util.List filterGroups(java.util.List,boolean)</function-signature>
	</function>
</taglib>
# FIXME: do not start system wide properties with project use system instead

project.name  = BibLicious
# address INCLUDING the trailing slash!
project.home  = http://localhost:8080/

genealogy.import.user = genealogie
genealogy.activated = true

system.mode.readOnly = false
system.specialUsersTagMap = {"dblp": "dblp", "${genealogy.import.user}" : "dnb"}

# possible values: en,de,ru
system.locales.default = en
system.locales.supported = en,de,ru

# theme used in the webapp (possible values: bibsonomy, puma)
project.theme = bibsonomy
project.theme.help =

project.files.home = ${user.home}

# bookmark previews server (empty image server = disabled previews)
project.img =
project.blog  = https://blog.bibsonomy.org

# social media links
project.social.twitter = bibsonomyCrew
project.social.facebook =
project.social.youtube =
project.social.instagram =

project.email = webmaster@bibsonomy.org
project.noSpamEmail = no-spam@bibsonomy.org
project.registrationFromAddress = register@bibsonomy.org
project.joinGroupRequestFromAddress = groups@bibsonomy.org
project.apiEmail = api-support@bibsonomy.org

# document paths
project.document.allowedExtensions = pdf, ps, djv, djvu, txt, tex, doc, docx, ppt, pptx, xls, xlsx, ods, odt, odp, jpg, jpeg, svg, tif, tiff, png, htm, html, epub
project.document.path = ${project.files.home}/bibsonomy_docs/
project.document.temp = ${project.files.home}/bibsonomy_temp/
project.picture.path  = ${project.files.home}/bibsonomy_pics/

project.help.path = ${project.files.home}/help/

# FIXME: this property currently does not allow to disable one specific addon
project.addons.available = chrome,firefox,safari
#BibSonomy buttons
project.addons.chrome.url = https://chrome.google.com/webstore/detail/bibsonomy-buttons/lbbnooihfnhphbgeajgmpmaedkdjgeid
project.addons.firefox.url = https://addons.mozilla.org/firefox/addon/bibsonomy-buttons/
project.addons.safari.url = https://safari-extensions.apple.com/details/?id=org.bibsonomy.buttons-RQ8CG56FP3
#PUMA buttons
#project.addons.chrome.url = https://chrome.google.com/webstore/detail/puma-buttons/emdfjhapplcngnmhllpdmaecgoafjjmc
#project.addons.firefox.url = https://addons.mozilla.org/firefox/addon/puma-buttons/
#project.addons.safari.url = /resources/addons/safari/puma-buttons-extension/puma_buttons.safariextz

# Group creation mode (requestedbased or automatic)
project.groupcreationmode = requestedbased


# possible values: reCaptcha2, mockCaptcha
project.bean.captcha = mockCaptcha
# possible values: realPingback, mockPingback
project.bean.pingback = realPingback
# possible values: realSynchronizationClient, mockSynchronizationClient
project.bean.syncclient = realSynchronizationClient

project.urlScheme = ${project.theme}

system.cris = false
system.cris.college =
system.cris.project.links = true

#
# navigation
#
# possible values: user, posts, groups, organizations, publications, persons, projects, explore, popular
system.nav.order = user,posts,groups,publications,persons,popular
system.nav.order.cris = user,posts,persons,organizations,publications,projects

# a system wide OpenURL provider
system.openurl.provider =

#
# recaptcha 2 configuration
#
recaptcha2.secretKey = secretKey
recaptcha2.siteKey = sitekey
recaptcha2.server = https://www.google.com/recaptcha/api/siteverify


# Memento configuration
memento.timegate = http://timetravel.mementoweb.org/memento

# full text search configuration
# update the search index
search.update.enabled = true

# Elasticsearch IP and port, if we have multiple addresses, they
# will be separated by "," and port and ip are separated by ":"
search.es.address = localhost:9200

# SWORD Configuration
publication.reporting.mode = ALL
publication.reporting.enabled = false

sword.dirTemp = /tmp/
sword.httpServer = host.domain.tld
sword.httpPort = 8080
sword.httpUserAgent = PUMA-UA
sword.httpServicedocumentUrl = /sword/servicedocument
sword.httpDepositUrl = http://host.domain.tld:port/sword/deposit/urn:nbn:de:abc:34-1234
sword.authUsername = username
sword.authPassword = password

mail.smtp.host = localhost

cron.fulltextsearch.regenerate.enabled = true
cron.fulltextsearch.regenerate.enabled.publication = ${cron.fulltextsearch.regenerate.enabled}
cron.fulltextsearch.regenerate.enabled.bookmark = ${cron.fulltextsearch.regenerate.enabled}

#
# authentication settings
#
# internal: database, ldap: ldap server, openid: open id server, x509, saml (for shibboleth)
auth.order = internal,openid,httpbasic
# disables the registration (currently only the link-rendering), this is used by some pumas
auth.register.disabled = false
# sets the preferred login method (shown on homepage and in the navigation bar, no effect on /login)
auth.preferred.login =

# SAML register fields (possible values: realname, gender, place, homepage
auth.register.saml.fields.required = 
auth.register.saml.fields.optional = realname,gender,place,homepage
auth.register.saml.forceRemoteIDAsUsername = false

#
# LDAP Config
# configure the LDAP authentication provder - possible options adAuthenticationProvider or ldapAuthProvider
auth.ldap.authenticationProvider = ldapAuthProvider

# server; don't forget to set the rootDn
auth.ldap.server = ldap://127.0.0.1:33389/
# possible values: simpleLdapBindAuthenticator, searchLdapBindAuthenticator
auth.ldap.authenticator = simpleLdapBindAuthenticator
# when using the simpleLdapBindAuthenticator: the pattern to build the user DN
auth.ldap.userDnPattern = uid={0},ou=pica-users
# when using the searchLdapBindAuthenticator:   
auth.ldap.userSearchBase = 
auth.ldap.userSearchFilter = (&(objectclass=user)(|(ou:dn:=people)(ou:dn:=user))(uid={0})(!(ou:dn:=alumni))(!(ou:dn:=deleted))(!(ou:dn:=private)))
# shall the LDAP ID be used as suggestion for a user name? 
auth.ldap.ldapIdIsUsername = false 

auth.cookie.cryptkey = 37d7f7f81e341bd3a05fc5ae9a1ec4c9

# AD Config
# domain the domain name (may be null or empty)
auth.ad.domain = corp.example.com
# url an LDAP url (or multiple URLs)
auth.ad.url = ldap://127.0.0.1

# a name that appears in the auto-generated saml metadata (historically bib_test2 but should be set to something nice)
auth.saml.entityalias=bib_test2
# Points to a keystore which contains the private key of the Shibboleth Service Provider
auth.saml.keystore = 
# password for the keystore
auth.saml.keystore.pass = 
# Alias-name of the key in the store
auth.saml.keystore.key.alias = 
# Password of the key in the keystore
auth.saml.keystore.key.pass = 
# The entity name of the used IdentityProvider from the metadata file
auth.saml.defaultIdp =
# name of the attribute from Shibboleth, that contains the remote-user-name
auth.saml.attr.userId =
# use https consumerservice urls in metadata and for callback urls inside requests which are sent to the idp
auth.saml.useHttps = false
# mapping saml attributes to our user model (e.g. realname)
auth.saml.attr.map = {}
# configures the post binding to use, valid values are postBindingSaml1 and postBindingSaml2
auth.saml.binding.post = postBindingSaml1

# configure the metadata provider (possible values filesystemMetadataProvider, httpMetadataProvider)
auth.saml.metadata.provider = filesystemMetadataProvider
# A file containing the metadata of the Shibboleth Identity Provider (and other IdPs und SPs)
auth.saml.idp.1.metadata.file =
# url for the httpMetadataProvider
auth.saml.idp.1.metadata.url = 

# configure prechecker (possible defaultSAMLPreChecker, requiredAttributesChecker)
auth.saml.prechecker = defaultSAMLPreChecker
auth.saml.prechecker.requiredAttribures = {}

# json map structure mapping from parameter values given to http://localhost:8080/logout?redirectKey= to redirect urls
logout.redirect.map = {}

# URL to inform a remote-system (vuFind) about a limited user activation
limited_account.redirect_url = 

# URLs appearing in bibtex field 'biburl' for exports linked from vufind
export.bibtex.vufind.marc.biburl = 
export.bibtex.vufind.ebsco.biburl = 

# secret key to encrypt/decrypt reminder hashes (during password reminder function)
password.reminder.cryptkey = abcdefg

# controls the news displayed on the homepage
news.group = dmir
news.tag = bibsonomynews

# comma separated list holding the consumer keys of OAuth clients that should not be deletable
invisible.oauth.consumers = 

# the maximum amount of posts that can be queried at once
database.maxQuerySize = 1000

# the datasource class to use
database.datasource.class = org.apache.tomcat.dbcp.dbcp2.BasicDataSource

database.mysql.default.config = autoReconnect=true&useUnicode=true&characterEncoding=utf-8&mysqlEncoding=utf8&zeroDateTimeBehavior=convertToNull
# database settings
# main database
database.main.driverClassName = com.mysql.jdbc.Driver
database.main.url = jdbc:mysql://biblicious.org/bibsonomy?${database.mysql.default.config}
database.main.maxIdle = 50
database.main.maxTotal = 260
database.main.maxWait = 10000
database.main.removeAbandoned = true
database.main.removeAbandonedTimeout = 300
database.main.username = bibsonomy
database.main.password = 

# slave database
database.slave.driverClassName = ${database.main.driverClassName}
database.slave.url = ${database.main.url}
database.slave.username = ${database.main.username}
database.slave.password = ${database.main.password}

# clicklogger database
database.clicklogger.driverClassName = ${database.main.driverClassName}
database.clicklogger.url = ${database.main.url}
database.clicklogger.maxIdle = ${database.main.maxIdle}
database.clicklogger.maxTotal = ${database.main.maxTotal}
database.clicklogger.maxWait = ${database.main.maxWait}
database.clicklogger.removeAbandoned = ${database.main.removeAbandoned}
database.clicklogger.removeAbandonedTimeout = ${database.main.removeAbandonedTimeout}
database.clicklogger.username = ${database.main.username}
database.clicklogger.password = ${database.main.password}

# tag recommender database
database.recommender.tag.driverClassName = ${database.main.driverClassName}
database.recommender.tag.url = jdbc:mysql://biblicious.org/bibsonomy_recommender?${database.mysql.default.config}
database.recommender.tag.maxIdle = ${database.main.maxIdle}
database.recommender.tag.maxTotal = ${database.main.maxTotal}
database.recommender.tag.maxWait = ${database.main.maxWait}
database.recommender.tag.removeAbandoned = ${database.main.removeAbandoned}
database.recommender.tag.removeAbandonedTimeout = ${database.main.removeAbandonedTimeout}
database.recommender.tag.username = recommender
database.recommender.tag.password = 

# item recommender database
database.recommender.item.driverClassName = ${database.main.driverClassName}
database.recommender.item.url = jdbc:mysql://biblicious.org/bibsonomy_item_recommender?${database.mysql.default.config}
database.recommender.item.maxIdle = ${database.main.maxIdle}
database.recommender.item.maxTotal = ${database.main.maxTotal}
database.recommender.item.maxWait = ${database.main.maxWait}
database.recommender.item.removeAbandoned = ${database.main.removeAbandoned}
database.recommender.item.removeAbandonedTimeout = ${database.main.removeAbandonedTimeout}
database.recommender.item.username = ${database.recommender.tag.username}
database.recommender.item.password = ${database.recommender.tag.password}

# search database (only read only; maybe slave for performance)
database.search.driverClassName = ${database.main.driverClassName}
database.search.url = ${database.main.url}
database.search.maxIdle = ${database.main.maxIdle}
database.search.maxTotal = ${database.main.maxTotal}
database.search.maxWait = ${database.main.maxWait}
database.search.removeAbandoned = ${database.main.removeAbandoned}
database.search.removeAbandonedTimeout = ${database.main.removeAbandonedTimeout}
database.search.username = ${database.main.username}
database.search.password = ${database.main.password}

# opensocial database
database.opensocial.driverClassName = ${database.main.driverClassName}
database.opensocial.url = jdbc:mysql://biblicious.org/bibsonomy_opensocial?${database.mysql.default.config}
database.opensocial.maxIdle = ${database.main.maxIdle}
database.opensocial.maxTotal = ${database.main.maxTotal}
database.opensocial.maxWait = ${database.main.maxWait}
database.opensocial.removeAbandoned = ${database.main.removeAbandoned}
database.opensocial.removeAbandonedTimeout = ${database.main.removeAbandonedTimeout}
database.opensocial.username = ${database.main.username}
database.opensocial.password = ${database.main.password}

# controls the default toClassify field of users. 0 means all are classified as non-spammers
user.defaultToClassify = 1

# specifies a bibliography username that can receive posts in it's inbox without being friend of the sender
bibliography.bibliographyUser =

# optional features of bibsonomy-webapp
groupsandfriendsfeature.activated = false

# these values shouldn't be overridden by production systems; only for development
# this setting allows you disable the cache, that is uses to generate the messages
dev.messagesource.cache = -1
# use this setting to use different elasticsearch indices for a different system (e.g. to use the biblicious index)
# please set the search.update.enabled to false to disable the update of the system
search.es.systemId = ${project.home}


# Configuration of the goldstandard publication page
goldstandard.service.mail = false

# Configuration for profile images
# show default profile images for every entity
profile.picture.default = true

# Configuration for the highlighted group explore page
# Make sure 'explore' is added to the navigation order
explore.group.id =

# Configuration for extended search dialog
extendedSearch.entrytypes = article,book,booklet,collection,conference,dataset,electronic,inbook,incollection,inproceedings,manual,mastersthesis,misc,patent,periodical,phdthesis,preamble,presentation,proceedings,standard,techreport,unpublished
extendedSearch.searchFields = title,author,editor,user,group,additionalKey,publisher,institution,organization,school,doi,isbn,issn,journal,series,edition,volume,language

# Default realname search for organizations overview
organizations.overview.default.realnames =
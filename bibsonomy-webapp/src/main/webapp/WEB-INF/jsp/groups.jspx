<?xml version="1.0" ?>
<jsp:root version="2.0"
		  xmlns="http://www.w3.org/1999/xhtml"
		  xmlns:jsp="http://java.sun.com/JSP/Page"
		  xmlns:c="http://java.sun.com/jsp/jstl/core"
		  xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
		  xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
		  xmlns:fn="http://java.sun.com/jsp/jstl/functions"
		  xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
		  xmlns:lists="urn:jsptagdir:/WEB-INF/tags/layout/parts/lists"
		  xmlns:rc="urn:jsptagdir:/WEB-INF/tags/resources/common"
		  xmlns:fontawesome="urn:jsptagdir:/WEB-INF/tags/layout/fontawesome"
>

	<jsp:directive.page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" session="true"/>

	<c:set var="requPathAndQuery" value="/${requPath}${requQueryString}"/>
	<c:set var="isGroupsPage" value="${requPath eq 'groups'}"/>
	<c:set var="isUserLoggedIn" value="${command.context.userLoggedIn}"/>
	<fmt:message key="navi.${requPath}" var="pageTitle"/>
	<c:choose>
		<c:when test="${isGroupsPage}">
			<c:set var="activeTab" value="groups"/>
		</c:when>
		<c:otherwise>
			<c:set var="activeTab" value="organizations"/>
		</c:otherwise>
	</c:choose>

	<layout:paneLayout requPath="${requPath}" headerMessageKey="navi.${requPath}" command="${command}"
					   pageTitle="${pageTitle}" activeTab="${activeTab}" noSidebar="${true}">
		<!--+
		 	| heading
		 	+-->
		<jsp:attribute name="heading">
			<c:if test="${isGroupsPage}">
				<parts:search pathMessageKey="navi.search" formAction="/search" formInputName="search" hideGroup="${isGroupsPage}"/>
			</c:if>
		</jsp:attribute>

		<!--+
		 	| content
		 	+-->
		<jsp:attribute name="content">
			<c:set var="startOverviewPrefix" value="${not empty properties['organizations.overview.default.realnames'] and not isGroupsPage}"/>
			<parts:searchHeader headerMessageKey="${requPath}.header" searchPlaceholderKey="${requPath}.search" startOverviewPrefix="${startOverviewPrefix}"
								requPathAndQuery="${requPathAndQuery}" command="${command}"/>

			<c:set var="type" value="${isGroupsPage ? 'groups' : 'organizations'}" />
			<c:set var="groupListCommand" value="${command.groups}" />

			<div id="${type}" class="list-result">
				<c:choose>
					<c:when test="${isGroupsPage}">
						<fmt:message var="noResultMessage" key="groups.noResult" />
					</c:when>
					<c:otherwise>
						<fmt:message var="noResultMessage" key="organizations.noResult" />
					</c:otherwise>
				</c:choose>

				<c:choose>
					<c:when test="${groupListCommand.totalCount gt 0}">
						<c:set var="list" value="${groupListCommand.list}" />
						<c:set var="listLength" value="${fn:length(list)}" />
						<c:set var="columns" value="3" />

						<c:choose>
							<c:when test="${isGroupsPage}">
								<!--+
									| Groups overview
									+-->
								<c:forEach var="i" begin="0" end="${listLength - 1}" step="${columns}">
									<div class="row">
										<c:forEach var="offset" begin="0" end="${columns - 1}">
											<c:set var="position" value="${i + offset}" />
											<c:if test="${position lt listLength}">
												<lists:groupResultEntity columnSize="${12/columns}" group="${list[position]}"/>
											</c:if>
										</c:forEach>
									</div>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<!--+
									| Organization overview
									+-->
								<c:forEach var="i" begin="0" end="${listLength - 1}" step="${columns}">
									<div class="row">
										<c:forEach var="offset" begin="0" end="${columns - 1}">
											<c:set var="position" value="${i + offset}" />
											<c:if test="${position lt listLength}">
												<lists:organizationResultEntity columnSize="${12/columns}" organization="${list[position]}"/>
											</c:if>
										</c:forEach>
									</div>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<!-- No items found text shown -->
						<div class="no-items-found">
							<c:out value="${noResultMessage}" />
						</div>
					</c:otherwise>
				</c:choose>
			</div>
			<div id="${type}-result-footer" class="row list-result-footer">
				<!-- Show groups/organization pagination -->
				<div class="col-md-4">
					<rc:nextprev listView="${groupListCommand}" listViewStartParamName="groups.start"/>
				</div>
				<!-- Show list result -->
				<div class="col-md-4 vcenter">
					<lists:listResultInfo listCommand="${groupListCommand}" messageKey="${type}.results.description" />
				</div>
				<c:if test="${isGroupsPage}">
					<!-- Show create group button -->
					<div class="col-md-4 text-right vcenter">
						<fmt:message var="groupCreateHint" key="groups.create.hint"/>
						<a href="#" data-toggle="popover" data-placement="top" data-trigger="focus" data-content="${groupCreateHint}">
							<fontawesome:icon icon="info-circle" fixedWidth="${true}"/><c:out value=" "/>
						</a>
						<a class="btn btn-primary btn-sm" href="/grouprequest">
							<fmt:message key="groups.create"/>
						</a>
					</div>
				</c:if>
			</div>
		</jsp:attribute>

	</layout:paneLayout>
</jsp:root>
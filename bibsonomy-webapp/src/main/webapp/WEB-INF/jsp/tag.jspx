<?xml version="1.0" ?>
<jsp:root version="2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
	xmlns:sidebar="urn:jsptagdir:/WEB-INF/tags/layout/sidebar"
	xmlns:tags="urn:jsptagdir:/WEB-INF/tags/tags"
	xmlns:users="urn:jsptagdir:/WEB-INF/tags/users"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:fontawesome="urn:jsptagdir:/WEB-INF/tags/layout/fontawesome"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
	xmlns:nav="urn:jsptagdir:/WEB-INF/tags/nav">
	
	<jsp:directive.page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" session="true" />
	
	<c:set var="requPathAndQuery" value="/${requPath}${requQueryString}"/>
	
	<layout:resourceLayout pageTitle="${command.pageTitle}" command="${command}" requPath="${requPath}">
		<jsp:attribute name="breadcrumbs">
			<nav:breadcrumbs>
				<jsp:attribute name="crumbs">
					<nav:crumb nameKey="navi.tag" />
					<nav:tagcrumb requestedTags="${command.requestedTags}" active="${true}" />
				</jsp:attribute>
			</nav:breadcrumbs>
		</jsp:attribute>

		<jsp:attribute name="heading">
			<c:choose>
				<c:when test="${not empty command.scope and command.scope.name() ne 'LOCAL' and command.scope.name() ne 'SEARCHINDEX'}">
					<c:set var="pathMessageKey" value="navi.federatedTag"/>
					<c:url var="tagUrl" value="${relativeUrlGenerator.getTagUrlByTagName(command.requestedTags)}?scope=FEDERATED"/>
				</c:when>
				<c:otherwise>
					<c:set var="pathMessageKey" value="navi.tag"/>
					<c:url var="tagUrl" value="${relativeUrlGenerator.getTagUrlByTagName(command.requestedTags)}" />
				</c:otherwise>
			</c:choose>
			<parts:search scope="${command.scope}" pathMessageKey="${pathMessageKey}" formAction="/tag/" formInputName="tag" formInputValue="${command.requestedTags}" />
		</jsp:attribute>
		
		<jsp:attribute name="sidebar">
			<parts:tagsAndConceptsForSidebar/>
			
			<!-- related tags -->
			<c:if test="${not empty command.relatedTagCommand.relatedTags}">
				<sidebar:relatedTagsItem requPath="${requPath}" cmd="${command.relatedTagCommand}" scope="${command.scope.name()}" order="${command.sortKey.toString()}"/>
			</c:if>
			
			<fmt:message key="help.link.tag.similarRelated" var="helpLink"/>
			
			<!-- similar tags -->
			<c:if test="${not empty command.similarTags.relatedTags}">
				<sidebar:sidebarItem textKey="similartags" helpLink="${helpLink}">
					<jsp:attribute name="content">
						<tags:similar requPath="${requPath}" cmd="${command.similarTags}" scope="${command.scope.name()}"/>
					</jsp:attribute>
				</sidebar:sidebarItem>
			</c:if>
			
			<!-- related users -->
			<c:if test="${not empty command.relatedUserCommand.relatedUsers}">
				<sidebar:sidebarItem textKey="relatedusers">
					<jsp:attribute name="content">
						<users:related cmd="${command.relatedUserCommand}" userSimilarity="folkrank" displayFollowLink="${false}" scope="${command.scope.name()}"/>
					</jsp:attribute>
				</sidebar:sidebarItem>
			</c:if>
		</jsp:attribute>
	</layout:resourceLayout>

</jsp:root>
<?xml version="1.0" ?>
<jsp:root version="2.0"
          xmlns="http://www.w3.org/1999/xhtml"
          xmlns:jsp="http://java.sun.com/JSP/Page"
          xmlns:c="http://java.sun.com/jsp/jstl/core"
          xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
          xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
          xmlns:rc="urn:jsptagdir:/WEB-INF/tags/resources/common"
          xmlns:fn="http://java.sun.com/jsp/jstl/functions"
          xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
          xmlns:sidebar="urn:jsptagdir:/WEB-INF/tags/layout/sidebar"
>

    <jsp:directive.page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" session="true" />

    <c:set var="group" value="${command.group}" />
	<c:set var="groupName" value="${group.name}" />
	<c:set var="groupTitleName" value="${groupName}" />
	<c:if test="${not empty group.realname}" >
		<c:set var="groupTitleName" value="${group.realname}" />
	</c:if>

    <c:set var="pageTitle" value="Explore ${groupName}" />
    <c:set var="requPathAndQuery" value="/${requPath}${requQueryString}"/>

    <layout:paneLayout pageTitle="${pageTitle}"
                       command="${command}"
                       requPath="${requPath}"
					   noSidebar="${true}"
                       showPageOptions="${true}">

		<jsp:attribute name="headerExt">
			<script type="text/javascript" src="${resdir}/javascript/extendedSearch.js"><!-- keep me --></script>
			<script type="text/javascript" src="${resdir}/javascript/explorePage.js"><!-- keep me --></script>
			<script type="text/javascript" src="${resdir}/javascript/groupExplore.js"><!-- keep me --></script>
		</jsp:attribute>

        <jsp:attribute name="heading">
			<!-- nothing to display -->
		</jsp:attribute>

        <jsp:attribute name="content">
			<div id="groupExploreContent">
				<div class="row">
					<div class="col-md-12">
						<fmt:message var="headerMessage" key="explore.header">
							<fmt:param value="${fn:escapeXml(groupTitleName)}" />
						</fmt:message>
						<h1 id="requestedGroup" data-group="${groupName}"><c:out value="${headerMessage}" /></h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<p><c:out value="${group.description}" /></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-12">
								<parts:extendedSearch headerMessage="" searchPlaceholderKey="publications.search"
													  formAction="" formInputValue="${command.search}"
													  enableReset="${true}" alwaysShow="${true}" toggleFiltersOption="${true}"/>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="pull-right btn-group dropdown-align-right all-resources-menu">
									<rc:actions resourceType="bibtex"
												sortPage="pubdate"
												sortPageOrder="desc"
												entriesPerPage="20"
												showBookmark="${false}"
												showPublication="${true}"
												showInboxActions="${false}"
												isClipboard="${false}"
												disableFiltering="${true}"
												ignoreSortLinks="${true}"
												overridePath="/group/${groupName}" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<parts:spinner/>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div id="groupExplorePublications"><!-- keep me --></div>
							</div>
						</div>
					</div>
					<div class="col-md-3 sidebar">
						<div id="groupExploreFilterHeader">
							<!--<h3><fmt:message key="explore.filter.header"/></h3>-->
							<a id="resetFilterLink" href="#" onclick="resetFilterSelection()"><fmt:message key="explore.filter.reset"/></a>
						</div>
						<c:if test="${groupName eq properties['explore.group.id']}">
							<sidebar:searchFilterList filterField="tags" headerMessageKey="explore.filter.tags.header" headerMessageIcon="tags" hide="${true}" />
							<div id="filter-list-custom" class="filter-list hidden" data-field="tags">
								<sidebar:sidebarHeader textKey="explore.filter.custom.header" iconKey="sitemap"/>
								<fmt:message var="customFilterPlaceholder" key="explore.filter.search.placeholder" />
								<input id="searchCustomFilters" class="form-control" type="text" placeholder="${customFilterPlaceholder}" aria-label="${customFilterPlaceholder}" onkeyup="searchCustomFilters()" />
								<div id="filter-entries-custom" class="filter-entries search-filter-selection"><!-- keep me --></div>
							</div>
						</c:if>
						<c:if test="${command.filterMap.containsKey('entrytype')}">
							<sidebar:searchFilterList filterField="entrytype" filterEntries="${command.filterMap.get('entrytype')}"
													  headerMessageKey="explore.filter.entrytype.header" headerMessageIcon="book" buttonClass="filter-entry-counter" />
						</c:if>
						<c:if test="${command.filterMap.containsKey('year')}">
							<sidebar:searchFilterList filterField="year" filterEntries="${command.filterMap.get('year')}"
													  headerMessageKey="explore.filter.pubyear.header" headerMessageIcon="calendar-alt" buttonClass="filter-entry-counter" showMore="${true}" />
						</c:if>
					</div>
				</div>
			</div>
		</jsp:attribute>
    </layout:paneLayout>
</jsp:root>

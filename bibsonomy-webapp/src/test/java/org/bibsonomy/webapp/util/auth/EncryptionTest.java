/**
 * BibSonomy-Webapp - The web application for BibSonomy.
 *
 * Copyright (C) 2006 - 2021 Data Science Chair,
 *                               University of Würzburg, Germany
 *                               https://www.informatik.uni-wuerzburg.de/datascience/home/
 *                           Information Processing and Analytics Group,
 *                               Humboldt-Universität zu Berlin, Germany
 *                               https://www.ibi.hu-berlin.de/en/research/Information-processing/
 *                           Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               https://www.kde.cs.uni-kassel.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               https://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.webapp.util.auth;

import static org.junit.Assert.fail;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.text.BasicTextEncryptor;
import org.junit.Test;

/**
 * Simple test for encryption library.
 * 
 * @author dbenz
 */
public class EncryptionTest {

	private static final Log LOGGER = LogFactory.getLog(EncryptionTest.class);

	private static final String TEST_TEXT = "This is to be encrypted...";
	private static final String TEST_PASSWORD = "hallo";
	private static final String WRONG_PASSWORD = "halloooo";

	/**
	 * test encryption
	 */
	@Test
	public void testBasicTextEncryptor() {
		/*
		 * encrypt
		 */
		LOGGER.info("Starting encryption test; trying to encrypt '" + TEST_TEXT + "'");
		final String text = TEST_TEXT;
		BasicTextEncryptor enc = new BasicTextEncryptor();
		enc.setPassword(TEST_PASSWORD);
		final String crypted = enc.encrypt(text);
		LOGGER.info("Encrypted text: " + crypted);
		/*
		 * decrypt
		 */
		LOGGER.info("Decrypt again. Result: " + enc.decrypt(crypted));
		/*
		 * decrypt with wrong password
		 */
		LOGGER.info("Trying to decrypt with wrong password...");
		enc = new BasicTextEncryptor();
		enc.setPassword(WRONG_PASSWORD);
		try {
			enc.decrypt(crypted);
			fail("Encryption with wrong password should not be possible!");
		} catch (EncryptionOperationNotPossibleException ex) {
			// this should be the case
		}
	}

}

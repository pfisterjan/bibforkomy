/**
 * BibSonomy Recommendation - Tag and resource recommender.
 *
 * Copyright (C) 2006 - 2021 Data Science Chair,
 *                               University of Würzburg, Germany
 *                               https://www.informatik.uni-wuerzburg.de/datascience/home/
 *                           Information Processing and Analytics Group,
 *                               Humboldt-Universität zu Berlin, Germany
 *                               https://www.ibi.hu-berlin.de/en/research/Information-processing/
 *                           Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               https://www.kde.cs.uni-kassel.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               https://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.recommender.tag.simple;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.SortedSet;

import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.User;
import org.bibsonomy.recommender.connector.testutil.RecommenderTestContext;
import org.bibsonomy.recommender.tag.model.RecommendedTag;
import org.junit.Test;

/**
 * tests for {@link TagsOfPreviousPostsTagRecommender}
 *
 * @author dzo
 */
public class TagsOfPreviousPostsTagRecommenderTest {
	
	private static TagsOfPreviousPostsTagRecommender RECOMMENDER = RecommenderTestContext.getBeanFactory().getBean(TagsOfPreviousPostsTagRecommender.class);
	
	/**
	 * tests for {@link TagsOfPreviousPostsTagRecommender#getRecommendation(Post)}
	 */
	@Test
	public void testRecommendation() {
		final Post<Resource> post = new Post<>();
		post.setUser(new User("testuser1"));
		final SortedSet<RecommendedTag> tags = RECOMMENDER.getRecommendation(post);
		assertEquals(1, tags.size());
		assertThat(tags, contains(new RecommendedTag("weltmeisterschaft", 0, 0)));
	}
}
